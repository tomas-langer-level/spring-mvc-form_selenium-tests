package cz.langer.form.config;

import cz.langer.form.util.OsRecognizer;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Selenium configuration via direct downloaded webdrivers.
 * @author tomas.langer
 */
public class SeleniumConfig {

    @Getter
    private WebDriver driver;

    public SeleniumConfig() {
        ChromeOptions chromeOptions = new ChromeOptions();
        // Sets the headless mode to not open any browser window while testing.
        // This is useful when you plan to run selenium tests on server without any GUI.
//        chromeOptions.setHeadless(true);
        if (OsRecognizer.isWindows()) {
            // This is not working:
            // chromeOptions.setBinary("src/test/resources/chromedriver_win32/chromedriver.exe");
            // It throws:
            // java.lang.IllegalStateException: The path to the driver executable must be set by the webdriver.chrome.driver system property;
            // Must be set by this property:
            System.setProperty("webdriver.chrome.driver",
                    "webdriver/chromedriver_win32/chromedriver.exe");
        } else if (OsRecognizer.isUnix()) {
            System.setProperty("webdriver.chrome.driver",
                    "webdriver/chromedriver_linux64/chromedriver");
        } else if (OsRecognizer.isMac()) {
            System.setProperty("webdriver.chrome.driver",
                    "webdriver/chromedriver_mac64/chromedriver");
        }
        driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

}
