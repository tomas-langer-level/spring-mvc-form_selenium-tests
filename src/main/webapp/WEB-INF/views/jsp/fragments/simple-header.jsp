<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="cz.langer.form.common.Constants" %>

<head>
<title><%= Constants.TITLE %></title>

<spring:url value="/resources/core/css/main.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<spring:url value="/" var="urlHome" />

<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">Spring MVC Form</a>
        </div>
    </div>
</nav>