package cz.langer.form.common;

import cz.langer.form.annotation.TestUrl;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import cz.langer.form.config.SeleniumConfig;
import cz.langer.form.util.WebTestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

/**
 * Parent class for all Selenium tests. Do not run Selenium tests directly! Use "mvn verify" phase to do all selenium tests
 * as integration tests.
 * Selenium test can be run directly only in case when you run the web server on correct port manually for debug purposes.
 * All selenium tests derived from this class must match this naming convention "*IT.java" which is necessary to run
 * this test by maven-failsafe-plugin!
 *
 * @author tomas.langer
 */
@Slf4j
public abstract class SeleniumIT {

    protected SeleniumConfig config;
    private String url = null;

    /**
     * Gets URL for current test from {@link TestUrl} annotation above class which is child class of this class.
     * @return URL as String.
     */
    private String getUrl() {
        if (url != null) {
            return url;
        }
        TestUrl testUrl = this.getClass().getAnnotation(TestUrl.class);
        if (testUrl == null) {
            String errMsg = TestUrl.class.getSimpleName() + " annotation is mandatory for class " + this.getClass().getSimpleName();
            throw new IllegalArgumentException(errMsg);
        }
        url = testUrl.value();
        return url;
    }

    /**
     * Does all necessary operations BEFORE ALL Selenium tests.
     */
    @BeforeSuite
    public void beforeAllTests() {
    }

    /**
     * Does all necessary operations BEFORE EACH Selenium test CLASS.
     * @throws IOException
     * @throws InterruptedException
     */
    @BeforeClass
    public void setUp() throws IOException, InterruptedException {
        config = new SeleniumConfig();
        log.info("Testing URL: " + getUrl());
        config.getDriver().get(getUrl());
        assertEquals(WebTestUtils.getUrlResponseCode(getUrl()), HttpStatus.OK.value(), "Wrong HTTP status:");
    }

    /**
     * Does all necessary operations AFTER EACH Selenium test CLASS.
     */
    @AfterClass(alwaysRun = true)
    public void tearDown() {
        config.getDriver().quit();
    }

    /**
     * Does all necessary operations AFTER ALL Selenium tests.
     */
    @AfterSuite
    public void afterAllTests() {
    }

}
