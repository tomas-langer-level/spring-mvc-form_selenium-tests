INSERT INTO users (name, email, framework) VALUES ('mkyong', 'mkyong@gmail.com', 'Spring MVC, GWT');
INSERT INTO users (name, email, framework) VALUES ('alex', 'alex@yahoo.com', 'Spring MVC, PLAY');
INSERT INTO users (name, email, framework) VALUES ('joel', 'joel@gmail.com', 'Spring MVC, JSF 2');
INSERT INTO users (name, email, framework, address)
VALUES ('tomas', 'tomas@gmail.com', 'Spring MVC, JAVA, Javascript, jQuery, Postgress', 'Tomáš Langer, Hornická 19, Malé Svatoňovice, 542 34');