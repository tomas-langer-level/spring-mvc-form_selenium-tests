package cz.langer.form.web;

import cz.langer.form.model.User;
import cz.langer.form.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.LinkedList;
import java.util.List;

/**
 * @author tomas.langer
 */
@Controller()
@RequestMapping("/table-twins")
@Slf4j
public class TableTwinsController {

    private final UserService userService;

    public TableTwinsController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showPage(Model model) {
        log.debug("showPage");
//        model.addAttribute("users", userService.findAll());
        model.addAttribute("users", generateTestUsers());
        return "table-twins/table-twins-page";
    }

    private List<User> generateTestUsers() {
        LinkedList<User> users = new LinkedList<>();
        for (int i=1 ; i < 100 ; i++) {
            User user = new User();
            user.setId(i);
            user.setName("UserName" + i);
            user.setEmail("UserEmail" + i + "@gmail.com");
            users.add(user);
        }
        return users;
    }

}

