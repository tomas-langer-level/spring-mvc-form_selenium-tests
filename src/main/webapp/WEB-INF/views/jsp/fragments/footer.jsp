<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="container">
	<hr>
	<footer>
		<p>&copy; tomas.langer 2019</p>
	</footer>
</div>

<spring:url value="/resources/core/js/main.js" var="coreJs" />
<spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
<spring:url value="/resources/core/js/clipboard.js" var="clipboardJs" />
<spring:url value="/resources/core/js/jquery-3.4.1.min.js" var="jqueryJs" />

<script src="${coreJs}"></script>
<script src="${jqueryJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${clipboardJs}"></script>

<!-- Instantiate clipboard by passing a string selector -->
<script>
    var clipboard = new ClipboardJS('.btn-copy-to-clipboard');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>


