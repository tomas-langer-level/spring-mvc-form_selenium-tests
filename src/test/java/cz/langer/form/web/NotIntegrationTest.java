package cz.langer.form.web;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

/**
 * Test which serves only as verification that this test IS RUN only by "maven-surefire-plugin" and
 * otherwise NOT RUN by "maven-failsafe-plugin", because it isn't integration test.
 * @author tomas.langer
 */
@Slf4j
public class NotIntegrationTest {

    @Test
    public void test() {
        log.warn("THIS IS NOT INTEGRATION TEST, SO IF IT RUN BY \"maven-failsafe-plugin\" THEN OK, BUT IF RUN BY \"maven-surefire-plugin\" THEN IS NOT OK!");
    }
}
