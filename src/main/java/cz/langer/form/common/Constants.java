package cz.langer.form.common;

/**
 * @author tomas.langer
 */
public class Constants {

    /**
     * Title is also used in "header.jsp" and "simple-header.jsp"
     */
    public static final String TITLE = "Spring MVC Form Handling Example";

    private Constants() {
    }
}
