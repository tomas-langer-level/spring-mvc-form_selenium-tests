<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/simple-header.jsp" />

<body>

	<div class="container">

		<h1>Table Twins</h1>

        <%-- The "col-xs-x" class from bootstrap library does the trick with components side by side. --%>
        <div id="div1" class="col-xs-6 overflow:scroll">
            <table id="table1" class="table table-scrollable">
                <thead>
                    <tr>
                        <th class="col-xs-4">#ID</th>
                        <th class="col-xs-4">Name</th>
                        <th class="col-xs-4">Email</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${users}">
                        <tr id=${user.id} class="clickable-row">
                            <td class="col-xs-4">${user.id}</td>
                            <td class="col-xs-4">${user.name}</td>
                            <td class="col-xs-4">${user.email}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div id="div2" class="col-xs-6 overflow:scroll">
            <table id="table2" class="table table-scrollable">
                <thead>
                    <tr>
                        <th class="col-xs-4">#ID</th>
                        <th class="col-xs-4">Name</th>
                        <th class="col-xs-4">Email</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${users}">
                        <tr id=${user.id} class="clickable-row">
                            <td class="col-xs-4">${user.id}</td>
                            <td class="col-xs-4">${user.name}</td>
                            <td class="col-xs-4">${user.email}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>
</html>

<script>
$(document).ready(function(){
    console.info("jQuery is ready to go")
    $('#table1').on('click', '.clickable-row', function(event) {
        event.preventDefault();
        // Select row in this table
        $(this).addClass('active').siblings().removeClass('active');
        // Select row in other table
        var otherRow = $('#table2 #' + this.id);
        otherRow.addClass('active').siblings().removeClass('active');
        // Scroll to row in other table - FIFME: still not working
        /*
        var ypos = otherRow.offset().top;
        $('#div2').animate({
            scrollTop: $('#div2').scrollTop() + 100
        }, 500);
        */
        /*
        Mozna takto:
        var rowpos = $('#table tr:last').position();
        $('#container').scrollTop(rowpos.top);
        otherRow[0].scrollHeight je u řádku 6 = 622, takže to číslo je větší
        */       
        var pos = this.scrollHeight;
        $('#div2').scrollTop(pos)
        $('#div2').animate($('#div2').scrollTop(pos), 500);
    });
    $('#table2').on('click', '.clickable-row', function(event) {
        $(this).addClass('active').siblings().removeClass('active');
        $('#table1 #' + this.id).addClass('active').siblings().removeClass('active');
    });
});
</script>
