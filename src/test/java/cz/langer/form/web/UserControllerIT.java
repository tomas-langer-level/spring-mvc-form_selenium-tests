package cz.langer.form.web;

import cz.langer.form.annotation.TestUrl;
import cz.langer.form.common.Constants;
import cz.langer.form.common.SeleniumIT;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

/**
 * Selenium test of {@link UserController}
 * @author tomas.langer
 */
@Slf4j
@TestUrl("http://localhost:8090/spring-mvc-form/users")
public class UserControllerIT extends SeleniumIT {

    private List<WebElement> getFirstDataRowColumns(WebElement table) {
        WebElement firstTableDataRow = table.findElement(By.cssSelector("tbody > tr"));
        return firstTableDataRow.findElements(By.tagName("td"));
    }

    /**
     * Test of the page title.
     */
    @Test
    public void testTitle() {
        String title = config.getDriver().getTitle();
        assertNotNull(title, "Page title is null!");
        assertEquals(title, Constants.TITLE, "Unexpected title value");
        log.info("Page title: \"" + title + "\"");
    }

    /**
     * Test of users list table content.
     */
    @Test
    public void testTableColumnsCount() {
        WebElement table = config.getDriver().findElement(By.tagName("table"));
        List<WebElement> tableHeaderColumns = table.findElements(By.tagName("th"));
        // Selects first table data row and get its columns.
        List<WebElement> firstDataRowColumns = getFirstDataRowColumns(table);
        assertEquals(tableHeaderColumns.size(), 6, "Header columns count not match");
        assertEquals(tableHeaderColumns.size(), firstDataRowColumns.size(), "Header columns count and first data row column count not match");
    }

    /**
     * Test od redirection to page with user details via click on query button.
     * This test causes redirection to the URL with details, but another tests is for base URL specified by
     * {@link TestUrl} annotation under this class. So it is necessary to run this test as last via priority
     * settings, because redirection to base path before each test is unnecessary time expensive.
     */
    @Test(priority = 1)
    public void testQueryButton() {
        WebElement table = config.getDriver().findElement(By.tagName("table"));
        List<WebElement> firstDataRowColumns = getFirstDataRowColumns(table);
        String idStr = firstDataRowColumns.get(0).getText();
        WebElement queryButton = table.findElement(By.cssSelector("tbody > tr > td > button"));
        long stTime = System.currentTimeMillis();
        queryButton.click();
        log.info("Page load time after click = {} ms", (System.currentTimeMillis() - stTime));
        String currentUrl = config.getDriver().getCurrentUrl();
        String expectedUrlPart = "/users/" + idStr;
        String errMsg = "Url after click \"" + currentUrl + "\" not contains expected part \"" + currentUrl + "\"";
        assertTrue(currentUrl.contains(expectedUrlPart), errMsg);
        WebElement h1 = config.getDriver().findElement(By.tagName("h1"));
        log.info(h1.getText());
    }

}