package cz.langer.form.model;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class User {
	// form:hidden - hidden value
	Integer id;

	// form:input - textbox
	String name;

	// form:input - textbox
	String email;

	// form:textarea - textarea
	String address;

	// form:input - password
	String password;

	// form:input - password
	String confirmPassword;

	// form:checkbox - single checkbox
	boolean newsletter;

	// form:checkboxes - multiple checkboxes
	List<String> framework;

	// form:radiobutton - radio button
	String sex;

	// form:radiobuttons - radio button
	Integer number;

	// form:select - form:option - dropdown - single select
	String country;

	// form:select - multiple=true - dropdown - multiple select
	List<String> skill;

	public boolean isNew() {
		return (this.id == null);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		User user = (User) o;
		return newsletter == user.newsletter &&
				Objects.equals(id, user.id) &&
				Objects.equals(name, user.name) &&
				Objects.equals(email, user.email) &&
				Objects.equals(address, user.address) &&
				Objects.equals(password, user.password) &&
				Objects.equals(confirmPassword, user.confirmPassword) &&
				Objects.equals(framework, user.framework) &&
				Objects.equals(sex, user.sex) &&
				Objects.equals(number, user.number) &&
				Objects.equals(country, user.country) &&
				Objects.equals(skill, user.skill);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, email, address, password, confirmPassword, newsletter, framework, sex, number, country, skill);
	}

	public static void main(String[] args) {
		User user1 = new User();
		System.out.println("null: " + user1.equals(null));
		User user2 = new User();
		System.out.println("user1 hash: " + user1.hashCode());
		System.out.println("user2 hash: " + user2.hashCode());
		System.out.println("new: " + user1.equals(user2));
		user2.setName("Tomas");
		System.out.println("user1 hash: " + user1.hashCode());
		System.out.println("user2 hash: " + user2.hashCode());
		System.out.println("chng: " + user1.equals(user2));
	}
}
