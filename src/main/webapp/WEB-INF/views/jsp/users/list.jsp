<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>

	<div class="container">

		<c:if test="${not empty msg}">
			<div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<strong>${msg}</strong>
			</div>
		</c:if>

		<h1>All Users</h1>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>#ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>framework</th>
					<th width="250">Action</th>
					<th>Address</th>
				</tr>
			</thead>

			<c:forEach var="user" items="${users}">
				<tr>
					<td>
						${user.id}
					</td>
					<td>${user.name}</td>
					<td>${user.email}</td>
					<td><c:forEach var="framework" items="${user.framework}" varStatus="loop">
						${framework}
    					<c:if test="${not loop.last}">,</c:if>
						</c:forEach></td>
					<td>
						<spring:url value="/users/${user.id}" var="userUrl" />
						<spring:url value="/users/${user.id}/delete" var="deleteUrl" /> 
						<spring:url value="/users/${user.id}/update" var="updateUrl" />

						<button class="btn btn-info" onclick="location.href='${userUrl}'">Query</button>
						<button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
						<button class="btn btn-danger" onclick="this.disabled=true;post('${deleteUrl}')">Delete</button>
                    </td>
                    <td>
                        <c:set var="maxLength" value="40" />
                        <c:choose>
                            <%-- If text is longer than maxLength then... --%>
                            <c:when test="${fn:length(user.address) gt maxLength}">
                                <%-- ...put whole text into "hidden" span --%>
                                <span id="address-${user.id}" class="hidden-clipboard-text">${user.address}</span>
                                <%-- ...show short version in other span --%>
                                <c:set var="remainLength" value="${fn:length(user.address) - maxLength}" />
                                <c:set var = "shortString"
                                       value = "${fn:substring(user.address, 0, maxLength)} ... (${remainLength} more chars not showing)"/>
                                <span>${shortString}</span>
                                <%-- ...and show copy to clipboard button --%>
                                <button class="btn-copy-to-clipboard" data-clipboard-target="#address-${user.id}">
                                    Copy whole text to clipboard
                                </button>
                            </c:when>
                            <c:otherwise>
                                ${user.address}
                            </c:otherwise>
                        </c:choose>
                    </td>
				</tr>
			</c:forEach>
		</table>

	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>
</html>

<script>
$(document).ready(function(){
	console.info("jQuery is ready to go")
});
</script>
