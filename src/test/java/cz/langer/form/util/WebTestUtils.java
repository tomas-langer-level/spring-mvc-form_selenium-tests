package cz.langer.form.util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author tomas.langer
 */
@Slf4j
public class WebTestUtils {

    private WebTestUtils() {
    }

    /**
     * Returns HTTP response code for given URL via curl command.
     * @param url
     * @return
     */
    public static int getUrlResponseCode(String url) throws IOException, InterruptedException {
        // curl -s -o /dev/null -w "%{http_code}" http://www.example.org/
        String output = execCmd("curl -s -o /dev/null -w \"%{http_code}\" " + url);
        // Remove all non numeric characters from curl command output.
        output = output.replaceAll("[^\\d]", "");
        return Integer.parseInt(output);
    }

    private static String execCmd(String cmd) throws java.io.IOException {
        if (log.isDebugEnabled()) {
            log.debug("executing cmd: " + cmd);
        }
        Process process = Runtime.getRuntime().exec(cmd);
        java.util.Scanner s = new java.util.Scanner(process.getInputStream()).useDelimiter(System.getProperty("line.separator"));
        StringBuilder sb = new StringBuilder();
        while(s.hasNext()) {
            String str = s.next();
            sb.append(str);
            if (log.isDebugEnabled()) {
                log.debug(str);
            }
        }
        String result = sb.toString();
        return result;
    }

}
