package cz.langer.form.web;

import cz.langer.form.annotation.TestUrl;
import cz.langer.form.common.Constants;
import cz.langer.form.common.SeleniumIT;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Selenium test of {@link TableTwinsController}
 * @author tomas.langer
 */
@Slf4j
@TestUrl("http://localhost:8090/spring-mvc-form/table-twins")
public class TableTwinsControllerIT extends SeleniumIT {

    @Test
    public void testTitle() {
        String title = config.getDriver().getTitle();
        assertNotNull(title, "Page title is null!");
        assertEquals(title, Constants.TITLE, "Unexpected title value");
        log.info("Page title: \"" + title + "\"");
    }

}